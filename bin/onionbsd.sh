#!/bin/sh -e
##################################################
###!!          ONIONBSD v0.2 TOOLBOX         !!###
###!!                   ---                  !!###
###!!   Copyright 2019 The OnionBSD Project  !!###
##################################################
# CONTENTS:
# 1. Variables
# 2. Checks
# 3. Download files
# 4. Build functions
# 5. Cleanup functions
# ---
# If you use this script to build your own
# derative, please sign your distribution,
# using the GPG key as pointed in BUILD.md

fail() { >&2 echo "$@" exit 1; }

###
# 1. VARIABLES
###

# Which mirror to use to fetch the sets
vmirror        = "https://cdn.openbsd.org/pub/OpenBSD"
# Declare which version of OpenBSD to use
vversion       = "6.5"
# The architecture to use to build
varch          = "amd64"
# The GPG key used to sign the build
vsignkey       = ""
# The blocksize to use for the image
vblocksize     = 512
# The size our resulting image should be
vimagesize     = "1900000000" # 1.9GB
# The vnd identifier to use
vnd            = "vnd0"
# The vnd device to use
vnddev         = "/dev/${vnd}a"
# The raw vnd device to use
vndrdev        = "/dev/r${vnd}a"
# The mountpoint for vnd to use
vmountpoint    = "${name}-fstmp"
# Declare the workdir
vworkdir       = $(pwd)/workdir
# Define the packages - base
vbasepkgs      = "tor git htop hotplug-diskmount"
# Define the packages - XFCE
vxfcepkgs_gui  = "xfce-extras "
vxfcepkgs_desk = "nano slim slim-themes"
vxfcepkgs_imgs = "darktable gimp inkscape"
vxfcepkgs_nets = "chromium iridium"
vxfcepkgs_medi = "sox vlc audacious audacious-plugins ristretto"
vxfcepkgs_font = "adobe-source-code-pro adobe-source-sans-pro adobe-source-serif-pro liberation-fonts freefont-ttf noto-fonts noto-emoji roboto-fonts"
# Define the download tool
vdownloader    = "ftp -C"

###
# 2. CHECKS
###
runningobsd() {
  if [ "$(uname)" = "OpenBSD" ]; then
    bob = "thebuilder"
  else
    printf "OnionBSD requires OpenBSD to build, exiting..."
    fail();
  fi
}

depscheck() {
}

checkroot() {
  if [ ]
}

###
# 3. DOWNLOAD FILES
###

downfiles() {
  downdir=site/home/root/pkg_cache
  todownload="HERE-COME-THE-PACKAGES-SAY-WHUT"

	if [ ! -d $downdir] then;
    printf "\t- Download directory doesn't exist, creating...\n"
    mkdir -p $outdir
      for p in $todownload; do
        if [ ! -f $downdir/$p ]; then
          ftp -C $PKG_PATH/$p
          mv $p $OUTDIR/
        fi
      done
  fi
  exit 0
}


###
# 4. BUILD FUNCTIONS
###

prepdisk() {
	# Define the filesystem size construction
	fssize=$(expr $imagesize / $blocksize )

	# Create the mountpoint if it doesn't exist yet
	if [ ! -d $mountpoint ]; then
		printf "\t- Mountpoint doesn't exist, creating...\n"
		mkdir -p ${mountpoint}
	else
		printf "\t- Mountpoint already exists, skipping creation\n"
	fi

	printf "\t- Pre-allocating the disk image, this might take some time...\n"

	# Use dd to create the disk image and prefill it with random BS
	dd if=/dev/urandom of=${tmpimg} bs=${blocksize} count=${fssize}

	# Configure the vnode alias
	vnconfig ${vnd} ${tmpimg}

	printf "\t- Writing a GPT disk layout\n"

	# Format with GPT, rather than MBR
	fdisk -iy -g -b 960 ${vnd}

	printf "\t- Creating the partitions within the image\n"

	# Create the partitions
	echo "a\n\n\n\n\nw\nq\n" | disklabel -E ${vnd}

	printf "\t- Formatting the file system\n"

	# Well, actually format the filesystem
	newfs ${vndrdev}
}

builddisk() {
  printf "\t- Mounting and copying files over\n"

  mount ${vnddev} ${mountpoint}
  # Copy over the files
  cp -r ${name}/* ${mountpoint}
  # Install the bootloader
  installboot -r ${mountpoint} ${vnd} /usr/mdec/biosboot /usr/mdec/boot

  # Unmount and cleanup
  umount ${mountpoint}
  vnconfig -u ${vnd}
  rm -r ${mountpoint}

  mv ${tmpimg} onionbsd-v0.1.fs
}

prepiso() {
  if [ ! -e ${vname}.iso ]; then
    # Download the iso itself
    $vdownloader "${vmirror}/${vversion}/${varch}/install${vversion}.iso"
    # Download the corresponding checksum
    $ vdownloader "${vmirror}/${vversion}/${varch}/SHA256"
    if [ "$(uname)" = "OpenBSD" ]; then
      sha256 -C SHA256 install${vversion.iso
      if [ $? -ne 0 ]; then
        printf "\t- ERROR: SHA mismatch!\n"
        exit 1
      else
        printf "\t- The checksum matches, yay!\n"
        mv install${vversion}.iso ${vname}.iso
      fi
    fi
  fi

  if [ ! -d ${vname} ]; then
    printf "\t- Extracting the downloaded ISO\n"
    mkdir -p loopdir
    mkdir -p ./${vname}
    vnconfig vnd0 ${vname}.iso
    mount -t cd9660 /dev/vnd0c loopdir/
    cp -r loopdir/* ./${vname}
    umount loopdir/
    vnconfig -u vnd0
    rm -r loopdir/
  fi

  printf "\t- Confiuring the site set\n"
  mkdir -p site/etc
  touch site/install.site
  chmod 755 site/install.site
  touch site/etc/rc.firsttime
  chmod 755 site/etc/rc.firsttime
}

buildiso() {
  printf "\t- Creating the ISO\n"
  if [ -n "$(command -v mkisofs)" ]; then
    mkisofs -r -no-emul-boot -b ${vversion}/${varch}/cdbr -c boot.catalog -o ${vworkdir}/onionbsd.iso ${vname}
  else
    genisoimage -r -no-emul-boot -b ${vversion}/${varch}/cdbr -c boot.catalog -o ${vworkdir}/onionbsd.iso ${vname}
  fi
}
