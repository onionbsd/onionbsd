# OnionBSD and your privacy

**OnionBSD** wants to empower it's users to enjoy online privacy, circumvent censorship and bash out tracking/profiling. Therefor, it only follows logic that the project _does NOT_ have any inbuilt trackers, phone home functionality or any kind of telemetry. However, the effectiveness mainly depends on the user and the capability to segment online identities.

[TODO: Tor usage guidelines]