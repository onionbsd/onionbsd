. /etc/ksh.kshrc
HISTFILE=~/.ksh_hist
HISTSIZE=3000
HISTCONTROL=ignoredumps
PS1='<$LOGNAME@$HOSTNAME:/${PWD#/}> '