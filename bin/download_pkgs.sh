#!/bin/sh
# Author:      thuban <thuban@yeuxdelibad.net>, h3artbl33d
# License:     MIT
# Description: Download all packages and theirs dependencies

. ./vars.conf

OUTDIR=site/home/root/pkg_cache

TODL="
HERE-COME-THE-PACKAGES-SAY-WHUT
"

mkdir -p $OUTDIR

if [ "$(uname)" = "OpenBSD" ]; then
    DLER="ftp -C"
else
    if [ -n "$(command -v curl)" ]; then
        DLER="curl -O -C -"
    elif [ -n "$(command -v wget)" ]; then
        DLER="wget --continue"
    fi
fi

for p in $TODL; do
    if [ ! -f $OUTDIR/$p ]; then
        $DLER $PKG_PATH/$p
        mv $p $OUTDIR/
    fi
done

exit 0