# ONIONBSD CHANGELOG
### Early changes before any tags

* **20190423**
  * Rebranded to OnionBSD

* **20190422**
  * Initial working version
  * Commits will be signed from now on with GPG key A401ADD8D71F158A