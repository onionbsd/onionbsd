![OnionBSD Logo](etc/logo.png)
# OnionBSD
### _Sane and resilient privacy OS_

OnionBSD is an operating system which aims at preserving your privacy, security and anonymity. It helps you to:

* use the internet anonymously and circumvent censorship: all connections to the internet are forced to go through the Tor network;
* leave no trace on the computer you are using unless you ask it explicitly;
* use state-of-the-art cryptographic tools to encrypt your files, emails and instant messaging.

Read more information [on the wiki](https://gitlab.com/onionbsd/onionbsd/wikis/home).