#!/bin/sh
# Build openbsd custom fs
# see : 
# https://github.com/openbsd/src/blob/master/distrib/amd64/iso/Makefile#L9
# http://undeadly.org/cgi?action=article&sid=20140225072408

. ./vars.conf

if [ "$(uname)" = "OpenBSD" ]; then
	echo "---"
	echo "* Rebuilding fs"

	TMPIMG="tmpimage.$$"
	BLOCKSIZE=512
	SIZE="1900000000"    # 1.9G
	FSSIZE=$(expr $SIZE / $BLOCKSIZE ) # 1.9G

	VND="vnd0"
	VND_DEV="/dev/${VND}a"
	VND_RDEV="/dev/r${VND}a"
	MOUNT_POINT="${NAME}-fstmp"

	mkdir -p ${MOUNT_POINT}

	echo "preparing disk image"
	dd if=/dev/zero of=${TMPIMG} bs=${BLOCKSIZE} count=${FSSIZE}
	vnconfig ${VND} ${TMPIMG}
	echo "writing gpt"
	fdisk -iy -g -b 960 ${VND}
	echo "creating slice"
	echo "a\n\n\n\n\nw\nq\n" | disklabel -E ${VND} 

	echo "runnng newfs"
	newfs ${VND_RDEV}
	mount ${VND_DEV} ${MOUNT_POINT}

	echo "Copy image files"
	cp -r ${NAME}/* ${MOUNT_POINT}
	installboot -r ${MOUNT_POINT} ${VND} /usr/mdec/biosboot /usr/mdec/boot

	umount ${MOUNT_POINT}
	vnconfig -u ${VND}
	rm -r ${MOUNT_POINT}

	mv ${TMPIMG} hads.fs

else
    echo "This script only works on OpenBSD"
fi