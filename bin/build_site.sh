#!/bin/sh
# Author:      thuban <thuban@yeuxdelibad.net>, h3artbl33d
# License:     MIT
# Description: Create siteXX.tgz

. ./vars.conf

if [ -d ./site ]; then
    echo "---"
    echo "* Create packages list to install"
    if [ -e ./site/etc/rc.firsttime ]; then
        echo "$PACKAGES" > ./site/etc/firsttime_packages
    fi

    echo "---"
    echo "* set installurl"
    echo "$MIRROR" > ./site/etc/installurl

    echo "---"
    echo "* Create siteXX.tgz"
    cd site
    tar cvzf ../site${VERS}.tgz *

    cd ..
    echo "* Copy custom siteXX.tgz"
    cp site${VERS}.tgz ${NAME}/${VERSION}/${ARCH}/site${VERS}.tgz 
fi

exit 0