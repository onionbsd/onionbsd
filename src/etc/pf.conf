##########################################################
###!!      ONIONBSD MAIN FIREWALL CONFIGURATION      !!###
###!!                   ---                          !!###
###!!   At a later stage, this config will force     !!###
###!!   any and all traffic over Tor.                !!###
##########################################################

table <martians> const persist counters file "/etc/pf.conf.table.martians"
table <ban> persist counters file "/etc/pf.conf.table.ban"

set loginterface egress
set optimization normal
set block-policy drop
set syncookies adaptive (start 25%, end 12%)
set skip on { lo0 }

queue outq on vio0 bandwidth 750M max 750M flows 1024 qlimit 1024 default

anchor "scrub" in {
 match in all scrub (no-df random-id max-mss 1440)
}

antispoof log quick for { (egress) lo0 }

anchor "block/all"
anchor "block/in-quick-scan" in proto tcp
anchor "block/in-quick-bad" in on egress
anchor "block/out-quick-bad" out on egress
load anchor "block" from "/etc/pf.conf.anchor.block"

anchor "external" on egress {
	###
	# Outbound
	###
	anchor out proto { tcp udp } from (egress) {
		# DNS
		pass log (user) proto { tcp udp } \
		to port domain \
		tag SELF_INET
		# NTP
		pass log (user) proto udp \
		to port ntp \
		user { _ntp root } \
		tag SELF_INET
		# DHCP
		pass log (user) proto udp \
		from port bootpc to port bootps \
		tag SELF_INET
		# WEB
		pass log (user) proto tcp \
		to port { http https } \
		tag SELF_INET
		# SSH, WHOIS
		pass log (user) proto tcp \
		to port { ssh whois } \
		group { wheel } \
		tag SELF_INET
    # TOR
    pass proto tcp \
    user _tor\
    group { _tor } \
    tag ONION 
	}

	###
	# Inbound
	###
	anchor in proto { tcp udp } to (egress) {
		# Example: WEB
		# pass log proto tcp \
		# to port { http https } \
		# keep state (max 500, max-src-conn-rate 100/10) \
		# tag INET_SELF
	}
}

anchor "icmp/ipv4-icmp" inet proto icmp
anchor "icmp/ipv6-icmp" inet6 proto icmp6
load anchor "icmp" from "/etc/pf.conf.anchor.icmp"