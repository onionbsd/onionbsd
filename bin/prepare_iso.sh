#!/bin/sh
# Author:      thuban <thuban@yeuxdelibad.net>, h3artbl33d
# License:     MIT
# Description: Download and extract openbsd iso.

. ./vars.conf

echo "---"
echo "* Downloading OpenBSD iso"


if [ "$(uname)" = "OpenBSD" ]; then
    DLER="ftp -C"
else
    if [ -n "$(command -v curl)" ]; then
        DLER="curl -O -C -"
    elif [ -n "$(command -v wget)" ]; then
        DLER="wget --continue"
    fi
fi

if [ ! -e ${NAME}.iso ]; then
    $DLER "${MIRROR}/${VERSION}/${ARCH}/install${VERS}.iso"
    echo "---"
    echo "* Checking iso"
    # check if iso fits SHA256
    $DLER "${MIRROR}/${VERSION}/${ARCH}/SHA256"
    if [ "$(uname)" = "OpenBSD" ]; then
        sha256 -C SHA256 install${V1}${V2}.iso
    else
        GOODSHA="$(grep install${VERS}.iso SHA256 |cut -d' ' -f4)"
        CURSHA="$(sha256sum install${VERS}.iso |cut -d' ' -f1)"
        test $GOODSHA = $CURSHA
    fi

    if [ $? -ne 0 ]; then
        echo "There is a problem with the downloaded iso. Run the script again and try do delete the previous downloaded file."
        exit 1
    else
        mv install${VERS}.iso ${NAME}.iso
    fi
fi


if [ ! -d ${NAME} ]; then
    echo "---"
    echo "* Extracting iso..."
    mkdir -p loopdir
    mkdir -p ./${NAME}
    if [ "$(uname)" = "OpenBSD" ]; then
        vnconfig vnd0 ${NAME}.iso
        mount -t cd9660 /dev/vnd0c loopdir/
        cp -r loopdir/* ./${NAME}
        umount loopdir/
        vnconfig -u vnd0
    else
        mount -o loop ${NAME}.iso loopdir
        cp -r loopdir/* ./${NAME}
        umount loopdir/
    fi
    rm -r loopdir/
fi

echo "---"
echo "* Preparing site configuration..."
mkdir -p site/etc
touch site/install.site
chmod 755 site/install.site
touch site/etc/rc.firsttime
chmod 755 site/etc/rc.firsttime


exit 0