#!/bin/sh
# Build openbsd custom iso

. ./vars.conf

echo "---"
echo "* Rebuilding iso"


if [ -n "$(command -v mkisofs)" ]; then
    mkisofs -r -no-emul-boot -b ${VERSION}/${ARCH}/cdbr -c boot.catalog -o ${CWD}/hads.iso ${NAME}
else
    genisoimage -r -no-emul-boot -b ${VERSION}/${ARCH}/cdbr -c boot.catalog -o ${CWD}/hads.iso ${NAME} 
fi