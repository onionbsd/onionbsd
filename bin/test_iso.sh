#!/bin/sh
# Auteur :      thuban <thuban@yeuxdelibad.net>
# licence :     MIT

# Description : test openbsd curstom iso
# Depends : qemu

if [ ! -f sio2.img ]; then
    qemu-img create -f qcow2 test.img 20G
    qemu-system-x86_64 -cdrom hads.iso -hda test.img -boot d -m 1G,slots=3,maxmem=4G
else
    qemu-system-x86_64 -hda test.img -m 1G,slots=3,maxmem=4G

fi
