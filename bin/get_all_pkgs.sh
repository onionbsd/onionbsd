#!/bin/sh
# Author:      thuban <thuban@yeuxdelibad.net>, h3artbl33d
# License:     MIT
# Description: Download all packages and theirs dependencies

. ./vars.conf

OUTDIR=site/home/root/pkg_cache

get_deps() {
    DEPS=$(pkg_info -f $1 | grep '^@depend' | cut -f 3 -d :)
    echo $DEPS
}

dl_pkgs() {
    DEPS=$(get_deps $1)
    for d in $DEPS; do
        if [ ! -f $OUTDIR/$d.tgz ]; then
            ftp -C -o $OUTDIR/$d.tgz $PKG_PATH/$d.tgz
            dl_pkgs $d
        fi
    done
    p=$(pkg_info $1 | head -n1 | cut -d' ' -f 3)
    # test if url in description -> package not installed
	if [ -n "$(echo $p |grep $PKG_PATH)" ]; then
		if [ ! -f $OUTDIR/$(basename $p) ]; then
			ftp -C -o $OUTDIR/$(basename $p) $p
		else
			echo "$p already downloaded"
		fi
	else
		p=$(echo $p | cut -d':' -f2)
		if [ ! -f $OUTDIR/$p.tgz ]; then
			ftp -C -o $OUTDIR/$p.tgz $PKG_PATH/$p.tgz
		else
			echo "$p already downloaded"
		fi
	fi
	if [ $? -ne 0 ]; then
		echo "download failed"
		exit 1
	fi

}

mkdir -p $OUTDIR

# quirks is needed
dl_pkgs "quirks"

for p in $PACKAGES; do
    echo ""
    echo "*** $p"
    dl_pkgs $p
done

exit 0