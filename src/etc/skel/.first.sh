#!/bin/sh

. ~/.profile

export XDG_CONFIG_DIRS=$HOME/.config/user-dirs.dirs
xdg-user-dirs-update

echo 'LC_CTYPE="en_US.UTF-8"' >> ~/.profile
echo 'LC_MESSAGES="en_US.UTF-8"' >> ~/.profile
echo 'LC_ALL="en_US.UTF-8"' >> ~/.profile
echo 'LANG="en_US.UTF-8"' >> ~/.profile
echo 'export LC_CTYPE LC_MESSAGES LC_ALL LANG' >> ~/.profile
MOUNTPOINTNAME="Disks"

ln -s /vol/ ~/${MOUNTPOINTNAME}

echo 'export MAILPILE_HOME=$HOME/.mailpile' >> ~/.profile

#sed -i -e "s;CHANGEME;$(xdg-user-dir MUSIC);" ~/.mpdconf

rm $0
exit 0